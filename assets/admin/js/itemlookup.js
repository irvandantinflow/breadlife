;(function($){

	$('#morelines').keypress(function() { 
		if(e.which == 13 || e.keycode == 13) {
			$("#findDataBtn").click();
		}
	});
	// ============================ DATATABLES INITIALIZATION ============================
	var oTable = $('#tblItem').dataTable({ 
                "sPaginationType": "bootstrap",           
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records per page",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                }, 
                "aoColumns": [ 
                        null,
						null,
                        null
                    ]
            });
	$('#tblItem_wrapper .dataTables_filter input').addClass("form-control input-small"); // modify table search input
	$('#tblItem_wrapper .dataTables_length select').addClass("form-control input-xsmall"); // modify table per page dropdown
    // ============================ DATATABLES INITIALIZATION ============================

    // ============================ CHOOSE ITEM EVENT ============================
	$('#reg').click(function() {
		var rowID = $("#rowID").val();
		if(!$("input[name='fitem_code']:checked").val()) {
			alert('Harap Pilih Produk terlebih dahulu');
			return false;
		}
		addLines();
		var itemID      = $("input[name='fitem_code']:checked").val();
        
		$('#datatables tbody tr:last').each(function(){    
			$(this).find('td:eq(2)').html($("input[name='fitem["+itemID+"][kode_produk]']").val());  
			$(this).find('td:eq(3)').html($("input[name='fitem["+itemID+"][nama_produk]']").val());  
			$(this).find('#kode_produk').prop("value", $("input[name='fitem["+itemID+"][kode_produk]']").val());
			$(this).find('#nama_produk').prop("value", $("input[name='fitem["+itemID+"][nama_produk]']").val());
		}); 
		$("#closeFindItem").click();
		$('#datatables tbody tr:last #quantity').focus();
	}); 
	// ============================ CHOOSE ITEM EVENT ============================

	// ============================ ADD NEW ROW IN DATATABLES EVENT ============================
	function addLines(){  
		// check item_code cannot empty in every line 
		$("#datatables tr.tblRowSO:last").each(function() { 
			if ($(this).find('td:eq(2)').html() != '') { 
				var i = $('#datatables tr.tblRowSO').length;
				var sName;
				$("#datatables tr.tblRowSO:last").clone().find("input").each(function() {
					if ($(this).prop('name').indexOf('quantity') > -1) sName = 'order['+i+'][quantity]';
					if ($(this).prop('name').indexOf('kode_produk') > -1) sName = 'order['+i+'][kode_produk]';
					if ($(this).prop('name').indexOf('nama_produk') > -1) sName = 'order['+i+'][nama_produk]';
					if ($(this).prop('name').indexOf('id_permintaan') > -1) sName = 'order['+i+'][id_permintaan]';
					if ($(this).prop('name').indexOf('id_permintaan_detail') > -1) sName = 'order['+i+'][id_permintaan_detail]';
					$(this).prop({
					  // 'id': function(_, id) { return sID },
					  'name': function(_, name) { return sName },
					  'value': ''               
					});
				}).end().appendTo("#datatables");
				 
				$("#datatables tr:last").each(function() {
					$(this).removeClass("danger");
				});
				
				var x=1;
				$("#datatables tr:last td").each(function() {
					var iRow = $(this).closest("tr").prevAll("tr").length;
					if (x!=1 && x!=2 && x!=3 && x!=4 && x!=5 && x!=6 && x!=7) $(this).html('');
					if (x==2) $(this).html(iRow+1);
					x++;
				});
				$("input:checkbox").uniform(); 
				$.uniform.update();
			}
		}); 
	} 
	// ============================ ADD NEW ROW IN DATATABLES EVENT ============================

	// ============================ SEARCH ITEMCODE / ITEMNAME EVENT ============================
	$('#findDataBtn').click(function(){
		var el = $(".ItemData");
		if ($("#findItem").val().trim() != '') { 
            App.blockUI(el);                    
			$.getJSON("searchItem/"+urlencode($("#findItem").val().trim()), {}, function(data) {
                var len = data.length; 
				if (len >= 1) { 
					oTable.fnClearTable();
					var i =0; 
                    
                    $.each(data, function(i, item){  
                        oTable.fnAddData([
                            '<input type="radio" value="'+i+'" name="fitem_code" id="fitem_code" />',
                            item.kode_produk + '<input type="hidden" value="' + escapeHtml(item.kode_produk) + '" name="fitem['+i+'][kode_produk]" id="fitem['+i+'][kode_produk]" />',
                            item.nama_produk +
                            '<input type="hidden" value="' + item.nama_produk + '" name="fitem['+i+'][nama_produk]" id="fitem['+i+'][nama_produk]" />'
                        ]); 
						i++;
					});
				} else  {
				    alert('Data Item Tidak Ditemukan'); 
				}
			})
            .done(function() { 
                window.setTimeout(function () {
                    App.unblockUI(el);
                }, 100);
                // el.preventDefault();
            });   
		}
		return false;
	});
	// ============================ SEARCH ITEMCODE / ITEMNAME EVENT ============================

	function urlencode (str) {
	  str = (str + '').toString();

	  return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').
	  replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
	}

	function escapeHtml(string) {
        return String(string).replace(/[&<>"'\/]/g, function (s) {
          return entityMap[s];
        });
      }

}(jQuery));