;(function($){
    $('#morelines').click(function() { 
		$("#ItemLookup").click();  
	});

	$('#ItemLookup').click(function() { 
		$("#findItem").val(''); 
		$("#tblItem tbody tr").remove(); 
	});

	$('#myModalItemLookup').on('shown.bs.modal', function () {
        $('#findItem').focus();
    })

    // ================== delete lines ================== 
	$("#deletelines").click(function(){ 
		if ($('#datatables >tbody >tr').length > 1) {
			$('#datatables tbody tr').each(function(){   
				if ($(this).find('input.lineitem:checked').length >= 1 && $('#datatables >tbody >tr').length > 1) {
					$(this).remove(); // to remove, use $(this).remove();
				}
			});
			
			//re-assign row number
			$('#datatables tbody tr').each(function(){
				var iRow = $(this).index();
				$(this).find('td:eq(1)').html(iRow+1);
				$(this).find('#quantity').attr("name", 'order['+iRow+'][quantity]');
				$(this).find('#kode_produk').attr("name", 'order['+iRow+'][kode_produk]');
				$(this).find('#nama_produk').attr("name", 'order['+iRow+'][nama_produk]');
				$(this).find('#id_permintaan').attr("name", 'order['+iRow+'][id_permintaan]');
				$(this).find('#id_permintaan_detail').attr("name", 'order['+iRow+'][id_permintaan_detail]');
			});
		} 
        
        if ($('#datatables >tbody >tr').length == 1) {
		    var iRow = 0; 
            $('#datatables tbody tr').each(function(){   
				if ($(this).find('input.lineitem:checked').length >= 1 && $('#datatables >tbody >tr').length == 1) { 
					$(this).find('td:eq(1)').html(iRow+1);
    			    $(this).find('td:eq(2)').html('');
    			    $(this).find('td:eq(3)').html('');
    			    $(this).find('#quantity').attr("name", 'order['+iRow+'][quantity]');
    			    $(this).find('#kode_produk').attr("name", 'order['+iRow+'][kode_produk]');
    			    $(this).find('#nama_produk').attr("name", 'order['+iRow+'][nama_produk]');
    			    $(this).find('#id_permintaan').attr("name", 'order['+iRow+'][id_permintaan]');
    			    $(this).find('#id_permintaan_detail').attr("name", 'order['+iRow+'][id_permintaan_detail]');
					$(this).find('#quantity').val('');
                    $(this).find('#kode_produk').val('');
                    $(this).find('#nama_produk').val('');
                    $(this).find('#id_permintaan').val('');
                    $(this).find('#id_permintaan_detail').val('');
				}
			}); 
		}
	});

    $('#initTable').click(function(){
		initializeTable($('#datatables'));
	});

	function initializeTable(jQtable) {
		jQtable.find("tr:gt(1)").remove();
		jQtable.find('tbody tr td:eq(1)').html(1);
		jQtable.find('tbody tr td:eq(2)').html('');
		jQtable.find('tbody tr td:eq(3)').html('');
		jQtable.find('tbody tr #quantity').attr("name", 'order[0][quantity]');
        jQtable.find('tbody tr #kode_produk').attr("name", 'order[0][kode_produk]');
        jQtable.find('tbody tr #nama_produk').attr("name", 'order[0][nama_produk]');
        jQtable.find('tbody tr #id_permintaan').attr("name", 'order[0][id_permintaan]');
        jQtable.find('tbody tr #id_permintaan_detail').attr("name", 'order[0][id_permintaan_detail]');
        jQtable.find('tbody tr #kode_produk').val('');
        jQtable.find('tbody tr #nama_produk').val('');
        jQtable.find('tbody tr #quantity').val('');
        jQtable.find('tbody tr #id_permintaan').val('');
        jQtable.find('tbody tr #id_permintaan_detail').val('');
	}

	$('#datatables .group-checkable').change(function () {
		var set = jQuery(this).attr("data-set");
		var checked = jQuery(this).is(":checked");
		jQuery(set).each(function () {
			if (checked) {
				$(this).attr("checked", true);
				$(this).parents('tr').addClass("active");
			} else {
				$(this).attr("checked", false);
				$(this).parents('tr').removeClass("active");
			}   			
		});
		jQuery.uniform.update(set);

	});

}(jQuery));