-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 12, 2018 at 04:46 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbs`
--

-- --------------------------------------------------------

--
-- Table structure for table `approval_detail`
--

CREATE TABLE IF NOT EXISTS `approval_detail` (
  `approval_detail_id` int(3) NOT NULL AUTO_INCREMENT,
  `approval_id` int(3) NOT NULL,
  `step` int(2) NOT NULL,
  `id_usergroup` int(3) NOT NULL,
  `status` char(1) NOT NULL,
  `remarks` text NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL,
  PRIMARY KEY (`approval_detail_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `approval_detail`
--

INSERT INTO `approval_detail` (`approval_detail_id`, `approval_id`, `step`, `id_usergroup`, `status`, `remarks`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(1, 1, 1, 2, 'W', '', 1, '2018-05-10 19:03:28', 0, '0000-00-00 00:00:00'),
(2, 1, 2, 5, 'W', '', 1, '2018-05-10 19:03:28', 0, '0000-00-00 00:00:00'),
(3, 1, 3, 6, 'W', '', 1, '2018-05-10 19:03:28', 0, '0000-00-00 00:00:00'),
(4, 1, 3, 7, 'W', '', 1, '2018-05-10 19:03:28', 0, '0000-00-00 00:00:00'),
(5, 2, 1, 2, 'W', '', 1, '2018-05-10 19:03:28', 0, '0000-00-00 00:00:00'),
(6, 2, 2, 5, 'W', '', 1, '2018-05-10 19:03:28', 0, '0000-00-00 00:00:00'),
(7, 2, 3, 6, 'W', '', 1, '2018-05-10 19:03:28', 0, '0000-00-00 00:00:00'),
(8, 2, 3, 7, 'W', '', 1, '2018-05-10 19:03:28', 0, '0000-00-00 00:00:00'),
(9, 3, 1, 2, 'W', '', 1, '2018-05-10 19:03:28', 0, '0000-00-00 00:00:00'),
(10, 3, 2, 5, 'W', '', 1, '2018-05-10 19:03:28', 0, '0000-00-00 00:00:00'),
(11, 3, 3, 6, 'W', '', 1, '2018-05-10 19:03:28', 0, '0000-00-00 00:00:00'),
(12, 3, 3, 7, 'W', '', 1, '2018-05-10 19:03:28', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `approval_header`
--

CREATE TABLE IF NOT EXISTS `approval_header` (
  `approval_id` int(3) NOT NULL AUTO_INCREMENT,
  `id_customer` int(3) NOT NULL,
  `doc_id` int(3) NOT NULL,
  `doc_type` varchar(100) NOT NULL,
  `curr_step` int(2) NOT NULL,
  `status` char(1) NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL,
  PRIMARY KEY (`approval_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `approval_header`
--

INSERT INTO `approval_header` (`approval_id`, `id_customer`, `doc_id`, `doc_type`, `curr_step`, `status`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(1, 18, 14, 'PC', 1, 'W', 1, '2018-05-10 19:03:28', 0, '0000-00-00 00:00:00'),
(2, 18, 4, 'MEMORANDUM', 1, 'W', 1, '2018-05-10 19:03:28', 0, '0000-00-00 00:00:00'),
(3, 18, 15, 'EDD', 1, 'W', 1, '2018-05-10 19:03:28', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE IF NOT EXISTS `branch` (
  `id_branch` int(3) NOT NULL AUTO_INCREMENT,
  `branch_name` varchar(50) NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL,
  PRIMARY KEY (`id_branch`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`id_branch`, `branch_name`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(1, 'Bogor', 1, '2018-04-14 11:28:27', 0, '0000-00-00 00:00:00'),
(2, 'Depok', 1, '2018-05-12 13:52:16', 0, '0000-00-00 00:00:00'),
(3, 'Jakarta', 1, '2018-05-12 13:52:28', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `id_customer` int(11) NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(50) DEFAULT NULL,
  `cif_no` varchar(100) DEFAULT NULL,
  `id_branch` int(3) DEFAULT NULL,
  `system_cif_open_date` datetime DEFAULT NULL,
  `report_date` date DEFAULT NULL,
  `next_review_date` date DEFAULT NULL,
  `created_user` int(3) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_user` int(3) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_customer`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id_customer`, `customer_name`, `cif_no`, `id_branch`, `system_cif_open_date`, `report_date`, `next_review_date`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(4, '123', '123', 1, '2018-05-05 00:00:00', '2018-05-05', '2019-05-05', 1, '2018-05-05 23:46:13', 1, '2018-05-10 11:33:01'),
(15, 'Irsyad Arifianto', '123', 1, '2018-05-06 00:00:00', '2018-05-06', '2019-05-06', 1, '2018-05-06 22:23:55', 1, '2018-05-10 11:32:41'),
(16, 'BBBB', 'CCCC', 1, '2018-05-10 00:00:00', '2018-05-10', '2019-05-10', 1, '2018-05-10 14:54:19', 1, '2018-05-10 18:08:16'),
(18, 'IRVAN AGUSSUSANTO', '11111', 1, '2018-05-10 00:00:00', '2018-05-10', '2019-05-10', 1, '2018-05-10 19:03:28', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `edd`
--

CREATE TABLE IF NOT EXISTS `edd` (
  `id_edd` int(3) NOT NULL AUTO_INCREMENT,
  `id_customer` int(11) NOT NULL,
  `status` char(1) NOT NULL,
  `report_date` date DEFAULT NULL,
  `review_date` date DEFAULT NULL,
  `pertemuan` varchar(200) DEFAULT NULL,
  `rubah_id` char(1) DEFAULT 'N',
  `serah_npwp` char(1) NOT NULL DEFAULT 'N',
  `pekerjaan` varchar(200) DEFAULT NULL,
  `latar_belakang` text,
  `tuj_milik` char(1) DEFAULT NULL,
  `tuj_milik_lain` varchar(50) DEFAULT NULL,
  `status_milik` char(1) DEFAULT NULL,
  `nama_dana_utama` varchar(50) DEFAULT NULL,
  `nomor_id` varchar(20) DEFAULT NULL,
  `nama_nasabah` varchar(50) DEFAULT NULL,
  `nomor_cif` varchar(20) DEFAULT NULL,
  `relasi` char(1) DEFAULT NULL,
  `relasi_lain` varchar(50) DEFAULT NULL,
  `dana_masuk` char(1) DEFAULT NULL,
  `dana_keluar` char(1) DEFAULT NULL,
  `transaksi_tunai` char(1) DEFAULT NULL,
  `cust_est_networth` double NOT NULL,
  `business_ownership` char(1) NOT NULL,
  `company_owner` varchar(100) NOT NULL,
  `annual_turnover` double NOT NULL,
  `lenght_owner` int(2) NOT NULL,
  `total_ownership` double NOT NULL,
  `employ_current` char(1) NOT NULL,
  `company_current` varchar(100) NOT NULL,
  `current_annual` double NOT NULL,
  `lenght_current` int(2) NOT NULL,
  `total_current` double NOT NULL,
  `inherintance` char(1) NOT NULL,
  `inherintance_from` varchar(100) NOT NULL,
  `relationship` varchar(100) NOT NULL,
  `amount_inheritence` double NOT NULL,
  `investments` char(1) NOT NULL,
  `investments_name` varchar(100) NOT NULL,
  `total_investments` double NOT NULL,
  `other` char(1) NOT NULL,
  `other_amount` double NOT NULL,
  `elaborate` text NOT NULL,
  `total` double NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL,
  PRIMARY KEY (`id_edd`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `edd`
--

INSERT INTO `edd` (`id_edd`, `id_customer`, `status`, `report_date`, `review_date`, `pertemuan`, `rubah_id`, `serah_npwp`, `pekerjaan`, `latar_belakang`, `tuj_milik`, `tuj_milik_lain`, `status_milik`, `nama_dana_utama`, `nomor_id`, `nama_nasabah`, `nomor_cif`, `relasi`, `relasi_lain`, `dana_masuk`, `dana_keluar`, `transaksi_tunai`, `cust_est_networth`, `business_ownership`, `company_owner`, `annual_turnover`, `lenght_owner`, `total_ownership`, `employ_current`, `company_current`, `current_annual`, `lenght_current`, `total_current`, `inherintance`, `inherintance_from`, `relationship`, `amount_inheritence`, `investments`, `investments_name`, `total_investments`, `other`, `other_amount`, `elaborate`, `total`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(4, 0, '', '2018-04-22', '2018-04-22', '99', 'N', '0', '99', '99', '1', '', '2', '', '', '88', '88', '1', '', '1', '2', '4', 30000000, '1', 'dantinflow', 100, 2, 210, '1', 'dantinflow', 100, 10, 1593.7424601, '1', '', '', 50, '0', '', 0, '0', 0, '', 1853.7424601, 1, '2018-04-15 20:38:33', 1, '2018-04-25 21:02:05'),
(5, 0, '', '2018-04-21', '2018-04-14', 'RGR', 'N', '0', '', '', '1', '', '0', '', '', '', '', '1', '', '2', '3', '3', 30000000, '1', 'XX', 1000000, 5, 6715610, '1', 'SS', 1000000, 12, 23522712.143931, '1', 'MR FRANCE', 'KAKEK', 3000000, '1', '', 1000000, '0', 0, '', 34238322.143931, 1, '2018-04-22 19:47:03', 0, '0000-00-00 00:00:00'),
(6, 4, '', '2018-05-05', '2019-05-05', 'asasas', 'N', '0', 'asasa', 'asasa', '1', '', '0', '', '', '', '', '1', '', '1', '1', '1', 500000000, '0', '', 0, 0, 0, '1', 'oke', 10000000, 5, 61051000, '0', '', '', 0, '0', '', 0, '0', 0, '', 61051000, 1, '2018-05-05 23:46:13', 1, '2018-05-10 11:33:01'),
(13, 15, '', '2018-05-06', '2019-05-06', 'oke', 'N', '0', '', '', '1', '', '0', '', '', '', '', '1', '', '1', '1', '1', 1000, '0', '', 0, 0, 0, '1', 'oke', 100, 12, 2138.428376721, '0', '', '', 0, '0', '', 0, '0', 0, '', 2138.428376721, 1, '2018-05-06 22:23:55', 1, '2018-05-10 11:32:41'),
(14, 16, 'W', '2018-05-10', '2019-05-10', 'S', 'N', '0', 'X', 'X', '1', '', '0', '', '', '', '', '1', '', '1', '1', '1', 10000, '1', 'X', 1000, 5, 6105.1, '1', 'X', 1000, 12, 21384.28376721, '0', '', '', 0, '0', '', 0, '0', 0, '', 27489.38376721, 1, '2018-05-10 14:54:19', 1, '2018-05-10 18:08:16'),
(15, 18, 'D', '2018-05-10', '2019-05-10', 'vrvrvr', 'N', '0', 'KERJA', 'rvrvr', '1', '', '0', '', '', '', '', '1', '', '1', '2', '3', 1000000, '1', 'DANTINFLOW', 1000000, 12, 21384283.76721, '1', 'DANTINFLOW OWNER', 1000000, 10, 15937424.601, '1', 'MR FRANCE', 'KAKEK', 10000000, '0', '', 0, '0', 0, '', 47321708.36821, 1, '2018-05-10 19:03:28', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `memorandum`
--

CREATE TABLE IF NOT EXISTS `memorandum` (
  `id_memorandum` int(3) NOT NULL AUTO_INCREMENT,
  `id_customer` int(11) NOT NULL,
  `status` char(1) NOT NULL,
  `date` date NOT NULL,
  `to` varchar(100) NOT NULL,
  `from` varchar(100) NOT NULL,
  `subject` varchar(200) NOT NULL,
  `background` text NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL,
  PRIMARY KEY (`id_memorandum`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `memorandum`
--

INSERT INTO `memorandum` (`id_memorandum`, `id_customer`, `status`, `date`, `to`, `from`, `subject`, `background`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(2, 0, '', '2018-04-19', 'asda', 'asda', 'asda', 'askjdkajlsdjalksjdlk akdsn asldnalsd', 1, '2018-04-15 09:19:49', 0, '0000-00-00 00:00:00'),
(3, 16, '', '2018-05-12', 'Mr Paulus Sutisna, President Director DBS Indonesia', 'Administrator ( Admin Bogor Branch )', 'Request for open relationship for Drs Wing Iskandar', 'KOKOK', 1, '2018-05-10 14:54:19', 1, '2018-05-10 18:08:16'),
(4, 18, 'D', '2018-05-10', 'Mr Paulus Sutisna, President Director DBS Indonesia', 'Administrator ( Admin Bogor Branch )', 'Request for open relationship for Drs Wing Iskandar', 'cecedcecd', 1, '2018-05-10 19:03:28', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `personal_customer`
--

CREATE TABLE IF NOT EXISTS `personal_customer` (
  `id_pc` int(11) NOT NULL AUTO_INCREMENT,
  `id_customer` int(11) DEFAULT NULL,
  `status` char(1) NOT NULL,
  `report_date` date DEFAULT NULL,
  `next_review_date` date DEFAULT NULL,
  `has_join_account` char(1) DEFAULT NULL,
  `risk_section_1_a` char(1) DEFAULT NULL,
  `risk_section_1_b` char(1) DEFAULT NULL,
  `risk_section_1_c` char(1) DEFAULT NULL,
  `risk_section_1_d` char(1) DEFAULT NULL,
  `risk_section_2_a` char(1) DEFAULT NULL,
  `risk_section_2_b` char(1) DEFAULT NULL,
  `risk_section_3_a` char(1) DEFAULT NULL,
  `risk_section_3_b` char(1) DEFAULT NULL,
  `risk_section_3_c` char(1) DEFAULT NULL,
  `risk_section_3_d` char(1) DEFAULT NULL,
  `risk_section_3_e` char(1) DEFAULT NULL,
  `risk_section_3_f` char(1) DEFAULT NULL,
  `risk_section_3_g` char(1) DEFAULT NULL,
  `risk_section_4_a` char(1) DEFAULT NULL,
  `risk_section_4_b` char(1) DEFAULT NULL,
  `risk_section_5` char(1) DEFAULT NULL,
  `risk_change_rating` char(1) DEFAULT NULL,
  `risk_change_rating_prev` char(1) DEFAULT NULL,
  `file_sr_1` text NOT NULL,
  `file_sr_2` text NOT NULL,
  `created_user` int(3) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_user` int(3) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_pc`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `personal_customer`
--

INSERT INTO `personal_customer` (`id_pc`, `id_customer`, `status`, `report_date`, `next_review_date`, `has_join_account`, `risk_section_1_a`, `risk_section_1_b`, `risk_section_1_c`, `risk_section_1_d`, `risk_section_2_a`, `risk_section_2_b`, `risk_section_3_a`, `risk_section_3_b`, `risk_section_3_c`, `risk_section_3_d`, `risk_section_3_e`, `risk_section_3_f`, `risk_section_3_g`, `risk_section_4_a`, `risk_section_4_b`, `risk_section_5`, `risk_change_rating`, `risk_change_rating_prev`, `file_sr_1`, `file_sr_2`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(4, 4, '', '2018-05-05', '2019-05-05', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', NULL, NULL, '484b0fceca347b0aa4f242595d30c34b.pdf', 'd390dd5ca2b5ab6398c567d4bae1543b.pdf', 1, '2018-05-05 23:46:13', 1, '2018-05-10 11:33:01'),
(12, 15, '', '2018-05-06', '2019-05-06', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', NULL, NULL, 'fd2d485a86dd15aae07212ce78c6ffd7.pdf', '04391384b4c815f10313b8e70ee33bf7.pdf', 1, '2018-05-06 22:23:55', 1, '2018-05-10 11:32:41'),
(13, 16, '', '2018-05-10', '2019-05-10', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '', NULL, NULL, 'd98eca7af54493ef2ee49c8362919f76.pdf', '05be97aa8633f303419a76bc71d7a7b0.pdf', 1, '2018-05-10 14:54:19', 1, '2018-05-10 18:08:16'),
(14, 18, 'D', '2018-05-10', '2019-05-10', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '3', NULL, NULL, 'b072c8bf28eaeb0b33a510dd9194e01b.jpeg', '22fc43c5011719f0c67cb855fe9707c9.jpg', 1, '2018-05-10 19:03:28', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(3) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `fullname` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `id_usergroup` int(3) NOT NULL,
  `aktif` char(1) DEFAULT 'Y',
  `id_branch` int(3) NOT NULL,
  `created_user` int(3) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_user` int(3) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `username`, `fullname`, `password`, `id_usergroup`, `aktif`, `id_branch`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(1, 'admin', 'Administrator', '21232f297a57a5a743894a0e4a801fc3', 1, 'Y', 1, NULL, NULL, NULL, NULL),
(2, 'ini', 'ini', '81dc9bdb52d04dc20036dbd8313ed055', 2, 'Y', 3, 1, '2018-04-10 01:38:10', 1, '2018-05-12 13:52:54'),
(3, 'app1', 'Approve 1', '81dc9bdb52d04dc20036dbd8313ed055', 2, 'Y', 1, 1, '2018-05-12 13:51:11', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `usergroup`
--

CREATE TABLE IF NOT EXISTS `usergroup` (
  `id_usergroup` int(3) NOT NULL AUTO_INCREMENT,
  `usergroup` varchar(30) NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL,
  PRIMARY KEY (`id_usergroup`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `usergroup`
--

INSERT INTO `usergroup` (`id_usergroup`, `usergroup`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(1, 'Administrator', 1, '2016-11-04 14:16:08', 1, '2018-05-10 16:26:21'),
(2, 'Business Process Improvement a', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(3, 'Branch Manager approve 1', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(4, 'Relationship Manager yang isi', 1, '2018-05-10 16:26:53', 0, '0000-00-00 00:00:00'),
(5, 'Consumer Banking Director appr', 1, '2018-05-10 16:27:03', 0, '0000-00-00 00:00:00'),
(6, 'Compliance Director approve 2', 1, '2018-05-10 16:27:15', 0, '0000-00-00 00:00:00'),
(7, 'President Director approve 3', 1, '2018-05-10 16:27:26', 0, '0000-00-00 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
