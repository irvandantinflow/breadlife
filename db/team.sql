-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 14, 2018 at 07:58 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `team`
--

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE IF NOT EXISTS `kategori` (
  `id_kategori` int(3) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(100) NOT NULL,
  `aktif` char(1) NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `kategori`, `aktif`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(3, 'DANISH', 'Y', 1, '2018-07-12 19:46:22', 1, '2018-07-12 19:46:53'),
(4, 'TOASH', 'Y', 1, '2018-07-12 19:50:28', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `kemasan`
--

CREATE TABLE IF NOT EXISTS `kemasan` (
  `id_kemasan` int(3) NOT NULL AUTO_INCREMENT,
  `kemasan` varchar(100) NOT NULL,
  `aktif` char(1) NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL,
  PRIMARY KEY (`id_kemasan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `kemasan`
--

INSERT INTO `kemasan` (`id_kemasan`, `kemasan`, `aktif`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(2, 'PCS', 'Y', 1, '2018-07-07 22:13:02', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `permintaan_detail`
--

CREATE TABLE IF NOT EXISTS `permintaan_detail` (
  `id_permintaan_detail` int(3) NOT NULL AUTO_INCREMENT,
  `id_permintaan` int(3) NOT NULL,
  `kode_produk` varchar(50) NOT NULL,
  `nama_produk` varchar(100) NOT NULL,
  `quantity` int(3) NOT NULL,
  `line_num` int(3) NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL,
  PRIMARY KEY (`id_permintaan_detail`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `permintaan_detail`
--

INSERT INTO `permintaan_detail` (`id_permintaan_detail`, `id_permintaan`, `kode_produk`, `nama_produk`, `quantity`, `line_num`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(1, 2, '130076', 'Cheese Le France', 1, 0, 1, '2018-07-14 22:20:38', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `permintaan_header`
--

CREATE TABLE IF NOT EXISTS `permintaan_header` (
  `id_permintaan` int(3) NOT NULL AUTO_INCREMENT,
  `permintaan_number` varchar(20) NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL,
  PRIMARY KEY (`id_permintaan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `permintaan_header`
--

INSERT INTO `permintaan_header` (`id_permintaan`, `permintaan_number`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(2, '1', 1, '2018-07-14 22:20:38', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE IF NOT EXISTS `produk` (
  `id_produk` int(3) NOT NULL AUTO_INCREMENT,
  `kode_produk` varchar(50) NOT NULL,
  `nama_produk` varchar(100) NOT NULL,
  `id_kategori` int(3) NOT NULL,
  `id_kemasan` int(3) NOT NULL,
  `aktif` char(1) NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL,
  PRIMARY KEY (`id_produk`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_produk`, `kode_produk`, `nama_produk`, `id_kategori`, `id_kemasan`, `aktif`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(2, '130076', 'Cheese Le France', 3, 2, 'Y', 1, '2018-07-12 19:48:43', 0, '0000-00-00 00:00:00'),
(3, '130050', 'Choco Bar', 3, 2, 'Y', 1, '2018-07-12 19:49:07', 0, '0000-00-00 00:00:00'),
(4, '130080', 'Danish Pretzel', 3, 2, 'Y', 1, '2018-07-12 19:49:38', 0, '0000-00-00 00:00:00'),
(5, '130138', 'Love Triangle', 3, 2, 'Y', 1, '2018-07-12 19:50:03', 0, '0000-00-00 00:00:00'),
(6, '130004', 'American Loaf', 4, 2, 'Y', 1, '2018-07-12 19:51:00', 0, '0000-00-00 00:00:00'),
(7, '130413', 'American Loaf Pocket Pack', 4, 2, 'Y', 1, '2018-07-12 19:51:33', 0, '0000-00-00 00:00:00'),
(8, '130440', 'Fujisan Choco', 4, 2, 'Y', 1, '2018-07-12 19:52:00', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(3) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `fullname` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `id_usergroup` int(3) NOT NULL,
  `aktif` char(1) DEFAULT 'Y',
  `created_user` int(3) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_user` int(3) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `username`, `fullname`, `password`, `id_usergroup`, `aktif`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(1, 'admin', 'Administrator', '21232f297a57a5a743894a0e4a801fc3', 1, 'Y', 1, '2018-06-14 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `usergroup`
--

CREATE TABLE IF NOT EXISTS `usergroup` (
  `id_usergroup` int(3) NOT NULL AUTO_INCREMENT,
  `usergroup` varchar(30) NOT NULL,
  `aktif` char(1) NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(3) NOT NULL,
  `updated_date` datetime NOT NULL,
  PRIMARY KEY (`id_usergroup`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `usergroup`
--

INSERT INTO `usergroup` (`id_usergroup`, `usergroup`, `aktif`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(1, 'Administrator', 'Y', 1, '2018-06-14 00:00:00', 0, '0000-00-00 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
