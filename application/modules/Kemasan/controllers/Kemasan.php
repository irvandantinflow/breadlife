<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kemasan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Kemasan_model');
		if(!is_logged_in()){redirect('Inadminpage');}
		// if(!is_access()){
		// 	$this->session->set_flashdata('error', 'Anda tidak berhak mengakses halaman ini');
		// 	redirect('Home');
		// }	
	}

	public function index(){	
		
	   	$data = array(
	   		'active'	=> 'Master',
	   		'content'	=> 'Kemasan/list_kemasan',
	   		'title'		=> 'Kemasan',
	   		'list'		=> $this->Kemasan_model->getAllList()
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	public function add(){	
		
	   	$data = array(
	   		'active'		=> 'Master',
	   		'content'		=> 'Kemasan/form_kemasan',
		   	'title'			=> 'New Kemasan',
	   		'page_action' 	=> base_url('Kemasan/save'),
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	function save(){
		$this->form_validation->set_rules('kemasan','Kode Kemasan', 'trim|required');

		$accept = $this->fungsi->accept_data(array_keys($_POST));

		if ($this->form_validation->run() == FALSE) { 
			
			$this->session->set_flashdata('error', validation_errors());
			redirect('Kemasan');

		} else {

			$data = array(
				'kemasan'			=> $accept['kemasan'],
				'aktif'				=> isset($accept['aktif']) ? $accept['aktif'] : 'N'
			);

			$query = $this->Kemasan_model->save($accept['id_kemasan'], $data);

			if($query)
				$this->session->set_flashdata('success', 'Data berhasil disimpan');
			else
				$this->session->set_flashdata('error', 'Data gagal disimpan');
			
			redirect('Kemasan');
		}	
	}

	public function delete($id){
		if (!$id) redirect('Kemasan');	
		$query = $this->Kemasan_model->delete($id);
		if($query)
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
		else
			$this->session->set_flashdata('error', 'Data gagal dihapus');
		
		redirect('Kemasan');
	}

	function edit($id = 0){	
		if(!$this->Kemasan_model->checkID($id)){
			$this->session->set_flashdata('error', 'Data tidak ditemukan');
			redirect('Kemasan');
		};

	   	$data = array(
	   		'active'		=> 'Master',
	   		'content'		=> 'Kemasan/form_kemasan',
	   		'title'			=> 'Edit Kemasan',
	   		'page_action' 	=> base_url('Kemasan/save'),
	   		'get_data'		=> $this->Kemasan_model->edit($id),
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}
}