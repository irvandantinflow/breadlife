<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produk_model extends CI_Model 
{    
	var $table = 'produk';
	var $primary = 'id_produk';

    public function __construct(){
        parent::__construct(); 
    }

    public function getList(){
        $query = $this->db->query("SELECT T0.id_produk, T0.kode_produk, T0.nama_produk, 
        							T1.kategori,
        							T2.kemasan,
        							CASE T0.aktif WHEN 'Y' THEN 'YA' ELSE 'TIDAK' END AS status_aktif 
        							FROM produk T0
        							LEFT JOIN kategori T1 ON T0.id_kategori = T1.id_kategori
        							LEFT JOIN kemasan T2 ON T0.id_kemasan = T2.id_kemasan
        							order by T0.kode_produk asc");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

    public function save($id ='', $data =''){

    	if(!empty($data)){
    		$this->db->where($this->primary, $id);
			$is_check = $this->db->get($this->table);

			if($is_check->num_rows() > 0){

				$this->db->set($data);
				$this->db->set('updated_user', $this->session->userdata('user_id'));
				$this->db->set('updated_date', 'now()', FALSE);
				$this->db->where($this->primary, $id);
				
				$query = $this->db->update($this->table);
				if($query)
					return true;
				else
					return false;
			} else {
				$this->db->set('created_user', $this->session->userdata('user_id'));
				$this->db->set('created_date', 'now()', FALSE);
				$query = $this->db->insert($this->table, $data);
				if($query)
					return true;
				else
					return false;
			}
    	}  	
    }

    public function delete($id){
		$query = $this->db->delete($this->table,array($this->primary=>$id));
		if($query)
			return true;
		else
			return false;
	}

	function checkID($id){
		$query = $this->db->get_where($this->table, array($this->primary=>$id));
		if ($query->num_rows() > 0) 
			return true;
		else
			return false;
    }

    public function edit($id){
		$query = $this->db->get_where($this->table, array($this->primary=>$id));
		return $query;
	}

	function getKategori(){
        $query = $this->db->query("SELECT id_kategori, kategori from kategori where aktif = 'Y' order by kategori asc");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

    function getKemasan(){
        $query = $this->db->query("SELECT id_kemasan, kemasan from kemasan where aktif = 'Y' order by kemasan asc");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

}   
