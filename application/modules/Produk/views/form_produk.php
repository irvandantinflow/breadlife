<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left"></div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group"></div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">

      <div class="col-md-6 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><?php echo $title; ?> </h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />
            <form class="form-horizontal form-label-left" method="POST" action="<?php echo $page_action;?>" >

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Kode Produk</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input type="text" class="form-control" placeholder="Kode Produk" name="kode_produk" value="<?php echo isset($get_data) ? $get_data->row()->kode_produk : ''; ?>">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Produk</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input type="text" class="form-control" placeholder="Nama Produk" name="nama_produk" value="<?php echo isset($get_data) ? $get_data->row()->nama_produk : ''; ?>">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Kategori</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <select class="form-control select2_single" name="id_kategori" required="">
                    <option value="">-- Select Kategori --</option>
                    <?php foreach ($kategori->result() as $baris) {
                      if (isset($get_data) && $baris->id_kategori == $get_data->row()->id_kategori) { ?>
                      <option value="<?php echo $baris->id_kategori; ?>" selected><?php echo strtoupper($baris->kategori); ?></option>
                       <?php } else { ?>
                      <option value="<?php echo $baris->id_kategori; ?>"><?php echo strtoupper($baris->kategori); ?></option>
                    <?php } }?>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Kemasan</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <select class="form-control select2_single" name="id_kemasan" required="">
                    <option value="">-- Select Kemasan --</option>
                    <?php foreach ($kemasan->result() as $baris) {
                      if (isset($get_data) && $baris->id_kemasan == $get_data->row()->id_kemasan) { ?>
                      <option value="<?php echo $baris->id_kemasan; ?>" selected><?php echo strtoupper($baris->kemasan); ?></option>
                       <?php } else { ?>
                      <option value="<?php echo $baris->id_kemasan; ?>"><?php echo strtoupper($baris->kemasan); ?></option>
                    <?php } }?>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Aktif</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <p>
                    YA <input type="radio" class="flat" name="aktif" value="Y" checked="" required <?php echo (isset($get_data) && $get_data->row()->aktif == 'Y') ? 'checked' : ''; ?>/> 
                    TIDAK <input type="radio" class="flat" name="aktif" value="N" <?php echo (isset($get_data) && $get_data->row()->aktif == 'N') ? 'checked' : ''; ?>/>
                  </p>
                </div>
              </div>

              <input type="hidden" name="id_produk" value="<?php echo isset($get_data) ? $get_data->row()->id_produk : ''; ?>"/>

              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                  <a href="<?php echo base_url('User'); ?>" class="btn btn-primary" name="cancel">Cancel</a>
                  <button type="submit" class="btn btn-success" name="submit">Submit</button>
                </div>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>