<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Permintaan_model extends CI_Model 
{    
	var $table = 'permintaan_header';
	var $primary = 'id_permintaan';

    public function __construct(){
        parent::__construct(); 
    }

    public function getList(){
        $query = $this->db->query("SELECT *,
                                    CASE WHEN status = 'O' THEN 'Open' WHEN status = 'C' THEN 'Close' ELSE '' END AS 'statuspermintaan',
                                    CASE WHEN pengiriman = '1' THEN 'Pagi' WHEN pengiriman = '2' THEN 'Siang' ELSE '' END AS 'waktupengiriman'
                                    FROM permintaan_header
                                    order by id_permintaan desc");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

    function addData($data_header = '', $data_detail = ''){
    
        $this->db->trans_start();
        
        // Insert Into permintaan_header Start
        $this->db->set('created_user', $this->session->userdata('user_id'));
        $this->db->set('created_date', 'now()', FALSE);
        $this->db->set($data_header);
        
        $query = $this->db->insert('permintaan_header');
        if(!$query) return false;
        $ID = $this->db->insert_id();
        // Insert Into permintaan_header End
        
        // Insert Into permintaan_detail Start
        for($i = 0; $i < count($data_detail); $i++){
            $this->db->set('id_permintaan', $ID, FALSE);
            $this->db->set('created_user', $this->session->userdata('user_id'));
            $this->db->set('created_date', 'now()', FALSE);
            $this->db->set($data_detail[$i]);
            
            $query = $this->db->insert('permintaan_detail');
            if(!$query) return false;
        }
        // Insert Into permintaan_detail End
        
        $this->db->trans_complete();
        
        return true;
    }

    function updateData($data_header = '', $data_detail = '', $idpermintaan = 0){

        $this->db->trans_start();

        // Update Into permintaan_header Start
        $this->db->set('updated_user', $this->session->userdata('user_id'), FALSE);
        $this->db->set('updated_date', 'now()', FALSE);
        $this->db->set($data_header); 
        $this->db->where('id_permintaan', $idpermintaan); 
        $query = $this->db->update('permintaan_header');
        if(!$query) return false;
        // Update Into permintaan_header End

        // Update Into permintaan detail Start
        $itd    = '';
        foreach($data_detail as $row) {
            $itDetailID = $row['id_permintaan_detail'];
            unset($row['id_permintaan_detail']);
            
            if ($itDetailID > 0) { 
                
                $itd    .= ($itd == '') ? $itDetailID : ','.$itDetailID; 
                $this->db->set('id_permintaan', $idpermintaan, FALSE);
                $this->db->set('updated_date', 'now()', FALSE);
                $this->db->set($row);
                $this->db->where('id_permintaan_detail', $itDetailID); 
                $query = $this->db->update('permintaan_detail'); 

            }else { 
                
                $this->db->set('id_permintaan', $idpermintaan, FALSE);
                $this->db->set('created_date', 'now()', FALSE);
                $this->db->set($row);
                $query = $this->db->insert('permintaan_detail'); 
                $ID = $this->db->insert_id();
                $itd    .= ($itd == '') ? $ID : ','.$ID; 

            } 
        }
        // Update Into permintaan_detail End

        if ($itd != '') {
            $this->db->query("DELETE FROM permintaan_detail where id_permintaan = ? and id_permintaan_detail not in (".$itd.")", $idpermintaan);
        }

        $this->db->trans_complete(); 

        return true;
    }

    function checkID($id){
        $query = $this->db->get_where($this->table, array($this->primary=>$id));
        if ($query->num_rows() > 0) 
            return true;
        else
            return false;
    }

    public function edit($id){
        $query = $this->db->query("SELECT *, CASE WHEN status = 'O' THEN 'Open' WHEN status = 'C' THEN 'Close' ELSE '' END AS 'statuspermintaan' FROM permintaan_header WHERE id_permintaan = '".$id."'");
        return $query;
    }

    public function editdetail($id){
        $query = $this->db->query("SELECT id_permintaan_detail, id_permintaan, kode_produk, nama_produk, quantity
                                    FROM permintaan_detail
                                    WHERE id_permintaan = '".$id."'
                                    order by line_num ");
        if($query->num_rows()>0){
            return $query;
        } else {
            $query->free_result();
            return $query;
        }
    }

    function search_item($id=''){

    	$ret = '';  
    	$sql = "
    			SELECT kode_produk, nama_produk
    			FROM produk
    			WHERE (kode_produk like '%".$id."%' or nama_produk like '%".$id."%')
    			";

    	$q = $this->db->query($sql);

    	if ($q->num_rows() > 0) {
    		foreach($q->result() as $row) {
                $ret[] = array("kode_produk" 	=> $row->kode_produk, 
                               "nama_produk" 	=> $row->nama_produk);
            }
    	}

    	return $ret;

    }
}   
