<div class="modal fade" id="myModalItemLookup" tabindex="-1" role="basic" aria-hidden="true"> 
    <?php echo $this->load->view('itemLookup');?> 
</div>

<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left"></div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group"></div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">

      <div class="col-md-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><?php echo $title; ?> </h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>
            <div class="clearfix"></div>
          </div>

          <form class="form-horizontal form-label-left" method="POST" action="<?php echo $page_action;?>" >
          <!-- HEADER SIDE -->
          <div class="x_content">
            <br />

              <!-- LEFT COLOUM -->
              <div class="col-md-6">

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">No. Document</label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="text" class="form-control" placeholder="No. Document" name="permintaan_number" value="<?php echo isset($get_data) ? $get_data->row()->permintaan_number : ''; ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="text" class="form-control" placeholder="Status" name="status" readonly="" value="<?php echo isset($get_data) ? $get_data->row()->statuspermintaan : ''; ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Revisi</label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="text" class="form-control" placeholder="Revisi" name="revisi" value="<?php echo isset($get_data) ? $get_data->row()->revisi : ''; ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Efektif</label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="text" class="form-control has-feedback-left datepicker" id="tanggal_efektif" name="tanggal_efektif" placeholder="Tanggal Efektif" aria-describedby="inputSuccess2Status3" value="<?php echo isset($get_data) ? date('d-m-Y',strtotime($get_data->row()->tanggal_efektif)) : ''; ?>" data-date-format="dd-mm-yyyy">
                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Jumlah Halaman</label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="text" class="form-control" placeholder="Jumlah Halaman" name="jumlah_halaman" value="<?php echo isset($get_data) ? $get_data->row()->jumlah_halaman : ''; ?>">
                  </div>
                </div>

              </div>
              <!-- LEFT COLOUM -->

              <!-- RIGHT COLOUM -->
              <div class="col-md-6">

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Contact Person</label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="text" class="form-control" placeholder="Nama Contact Person" name="contact_person" value="<?php echo isset($get_data) ? $get_data->row()->contact_person : ''; ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Outlet</label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="text" class="form-control" placeholder="Nama Outlet" name="outlet" value="<?php echo isset($get_data) ? $get_data->row()->outlet : ''; ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Pengiriman</label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="text" class="form-control has-feedback-left datepicker" id="tanggal_pengiriman" name="tanggal_pengiriman" placeholder="Tanggal Pengiriman" aria-describedby="inputSuccess2Status3" value="<?php echo isset($get_data) ? date('d-m-Y',strtotime($get_data->row()->tanggal_pengiriman)) : ''; ?>" data-date-format="dd-mm-yyyy">
                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Pengiriman</label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <select class="form-control select2_single" name="pengiriman" required="">
                      <option value="">-- Please Select --</option>
                      <option value="1" <?php if(isset($get_data) && $get_data->row()->pengiriman == "1") echo 'selected'; ?>>Pagi</option>
                      <option value="2" <?php if(isset($get_data) && $get_data->row()->pengiriman == "2") echo 'selected'; ?>>Siang</option>
                    </select>
                  </div>
                </div>

              </div>
              <!-- RIGHT COLOUM -->

          </div>
          <!-- HEADER SIDE -->

          <!-- DETAIL SIDE -->
          <div class="x_content">
            <br />

              <!-- LEFT COLOUM -->
              <div class="col-md-12">

                <div class="row">          
                  <div class="col-md-6">
                    <button type="button" name="deletelines" id="deletelines" class="btn btn-sm"><i class="fa fa-times"></i> Hapus Baris Yang Dipilih</button>
                  </div> 
                  <div class="col-md-6" style="text-align:right;">
                    <button type="button" name="initTable" id="initTable" class="btn btn-sm"><i class="fa fa-trash-o"></i> Hapus Seluruh Baris</button>
                    <button type="button" name="morelines" id="morelines" class="btn btn-sm"><i class="fa fa-plus"></i> Tambah Item</button>
                    <a name="ItemLookup" id="ItemLookup" class="btn btn-sm hide" href="#myModalItemLookup" data-toggle="modal" data-backdrop="static"><i class="fa fa-plus"> Tambah Item</i></a>
                  </div> 
                </div>

                <div class="row">
                  <!-- DETAIL-->
                  <div class="col-md-12"> 
                    <div class="table-scrollablexy">
                      <table class="table table-bordered table-advance table-hover" id="datatables">                        
                        <thead>
                          <tr>
                            <?php if(!isset($get_data) || isset($get_data) && $get_data->row()->statuspermintaan == "Open"){?>
                              <th style="width:8px"></th>
                            <?php } ?>
                            <th><span class="hidden-phone">No.</span></th>
                            <th><span class="hidden-phone">Kode Produk</span></th>
                            <th><span class="hidden-phone">Nama Produk</span></th>
                            <th><span class="hidden-phone">Quantity</span></th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                            $i = 1;
                            if (!empty($get_data_detail)){
                              foreach($get_data_detail->result() as $row){ 
                          ?>
                          <tr class="tblRowSO">
                            <?php if(!isset($get_data) || isset($get_data) && $get_data->row()->statuspermintaan == "Open"){?>
                              <td><input type="checkbox" class="checkboxes lineitem" value="" name="lineitem" /></td>
                            <?php } ?>  
                            <td><?php echo $i;?></td> 
                            <td><?php echo $row->kode_produk; ?></td> 
                            <td><?php echo $row->nama_produk; ?></td> 
                            <td>
                              <input type="text" name="order[<?php echo $i-1;?>][quantity]" class="txtbdu numeric" id="quantity" size="5" value="<?php echo (isset($row->quantity)) ? $row->quantity : ''; ?>" />
                              <input type="hidden" id="kode_produk" name="order[<?php echo $i-1;?>][kode_produk]" value="<?php echo (isset($row->kode_produk)) ? $row->kode_produk : ''; ?>" />
                              <input type="hidden" id="nama_produk" name="order[<?php echo $i-1;?>][nama_produk]" value="<?php echo (isset($row->nama_produk)) ? $row->nama_produk : ''; ?>" />
                              <input type="hidden" id="id_permintaan" name="order[<?php echo $i-1;?>][id_permintaan]" value="<?php echo (isset($row->id_permintaan)) ? $row->id_permintaan : ''; ?>" />
                              <input type="hidden" id="id_permintaan_detail" name="order[<?php echo $i-1;?>][id_permintaan_detail]" value="<?php echo (isset($row->id_permintaan_detail)) ? $row->id_permintaan_detail : ''; ?>" />
                            </td>
                          </tr>  
                          <?php
                            $i++;
                              }
                            } else {
                          ?>
                          <tr class="tblRowSO">
                            <?php if(!isset($get_data) || isset($get_data) && $get_data->row()->statuspermintaan == "Open"){?>
                              <td><input type="checkbox" class="checkboxes lineitem" value="" name="lineitem" /></td>
                            <?php } ?>
                            <td><?php echo $i;?></td> 
                            <td></td> 
                            <td></td> 
                            <td>
                              <input type="text" name="order[<?php echo $i-1;?>][quantity]" class="txtbdu numeric" id="quantity" size="5" value="" />
                              <input type="hidden" id="kode_produk" name="order[<?php echo $i-1;?>][kode_produk]" value="" />
                              <input type="hidden" id="nama_produk" name="order[<?php echo $i-1;?>][nama_produk]" value="" />
                              <input type="hidden" id="id_permintaan" name="order[<?php echo $i-1;?>][id_permintaan]" value="" />
                              <input type="hidden" id="id_permintaan_detail" name="order[<?php echo $i-1;?>][id_permintaan_detail]" value="" />
                            </td>
                          </tr>
                          <?php 
                            }
                          ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>

                <input type="hidden" name="id_permintaan" value="<?php echo isset($get_data) ? $get_data->row()->id_permintaan : ''; ?>"/>

                <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                    <a href="<?php echo base_url('Permintaan'); ?>" class="btn btn-primary" name="cancel">Cancel</a>
                    <?php if(!isset($get_data) || isset($get_data) && $get_data->row()->statuspermintaan == "Open"){?>
                      <button type="submit" class="btn btn-success" name="submit">Submit</button>
                    <?php } ?>
                  </div>
                </div>
              </div>
              <!-- LEFT COLOUM -->

          </div>
          <!-- DETAIL SIDE -->
          </form>

        </div>
      </div>
    </div>
  </div>
</div>

<!-- ADD AFTER jquery.min.js -->
<!-- <script src="<?php echo base_url();?>assets/admin/vendors/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/permintaan.js"></script> -->