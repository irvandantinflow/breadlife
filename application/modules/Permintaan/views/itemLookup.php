<div class="modal-dialog modal-wide" id="myModalItemLookup" style="width: 50% !important">
  <div class="modal-content"> 
    <div class="modal-header">
      <button class="close" onclick="" data-dismiss="modal">×</button> 
      <h4 class="modal-title">Cari Kode Produk</h4>
    </div> 
    <div class="modal-body" style="height:400px;">
      <div id="error_message" style="color:red"></div>
      <div class="row"> 
        <div class="col-md-8">
          <div class="form-group">
            <label class="col-md-4 control-label">Kode / Nama Produk</label>
            <div class="col-md-8">
                <input type="text" class="form-control" name="findItem" id="findItem" value=""/>
            </div> 
          </div> 
        </div> 
        <div class="col-md-4">
          <button class="btn btn-primary" id="findDataBtn" name="findDataBtn" value="findData" style="padding-right:20px;"><i class="fa fa-search"></i> Search</button>
        </div>
      </div>
      <div class="row ItemData">
        <div class="col-md-12">
          <table class="table table-striped table-bordered table-advance table-hover" id="tblItem">
            <thead>
              <tr>
                <th style="width:8px">&nbsp;</th>
                <th>Kode Produk</th>
                <th>Nama Produk</th>
              </tr>
            </thead>
            <tbody> 
            </tbody>
          </table> 
          <input type="hidden" id="rowID" name="rowID" value="" />
        </div>
      </div>
    </div>
    <div class="modal-footer">    
      <input class="btn btn-inverse" type="button" value="Pilih Item" id="reg" />
      <a href="#" onclick="" class="btn btn-default" data-dismiss="modal" id="closeFindItem">Close</a>                
    </div>   
  </div>
</div>