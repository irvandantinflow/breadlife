<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permintaan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Permintaan_model');
		if(!is_logged_in()){redirect('Inadminpage');}	
		// if(!is_access()){
		// 	$this->session->set_flashdata('error', 'Anda tidak berhak mengakses halaman ini');
		// 	redirect('Home');
		// }
	}

	public function index(){	
		
	   	$data = array(
	   		'active'	=> 'Transaction',
	   		'content'	=> 'Permintaan/list_permintaan',
	   		'list'		=> $this->Permintaan_model->getList()
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	public function add(){	
		
	   	$data = array(
	   		'active'		=> 'Transaction',
	   		'content'		=> 'Permintaan/form_permintaan',
		   	'title'			=> 'New Permintaan',
		   	'page_action' 	=> base_url('Permintaan/add_process'),
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	function add_process(){
		
		$data 	= $this->fungsi->accept_data(array_keys($_POST));

		//detail data
		$order = $data['order'];
		$i = 0;
		foreach($order as $row)
		{
			$data_detail[] = array(
				'line_num'			=> $i,
				'kode_produk'		=> $row['kode_produk'],
				'nama_produk'		=> $row['nama_produk'],
				'quantity'			=> $row['quantity']	
			);
			$i++; 
		}

		//header data
		$data_header = array(
			'permintaan_number'		=> $data['permintaan_number'],
			'status'				=> 'O',
			'revisi'				=> $data['revisi'],
			'tanggal_efektif'		=> isset($data['tanggal_efektif']) ? date('Y-m-d', strtotime($data['tanggal_efektif'])) : '',
			'jumlah_halaman'		=> $data['jumlah_halaman'],
			'contact_person'		=> $data['contact_person'],
			'outlet'				=> $data['outlet'],
			'tanggal_pengiriman'	=> isset($data['tanggal_pengiriman']) ? date('Y-m-d', strtotime($data['tanggal_pengiriman'])) : '',
			'pengiriman'			=> $data['pengiriman']
		);

		$query = $this->Permintaan_model->addData($data_header, $data_detail);
		
		if($query)
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
		else
			$this->session->set_flashdata('error', 'Data gagal disimpan');
		
		redirect('Permintaan');

	}

	function edit($id = 0){	
		if(!$this->Permintaan_model->checkID($id)){
			$this->session->set_flashdata('error', 'Data tidak ditemukan');
			redirect('User');
		};

	   	$data = array(
	   		'active'		=> 'Transaction',
	   		'content'		=> 'Permintaan/form_permintaan',
	   		'title'			=> 'Edit Permintaan',
	   		'page_action' 	=> base_url('Permintaan/update_process'),
	   		'get_data'		=> $this->Permintaan_model->edit($id),
	   		'get_data_detail'	=> $this->Permintaan_model->editdetail($id)
		);

	   	$this->template->load('tpl', $data['content'], $data);
	}

	function update_process(){
		
		$data 			= $this->fungsi->accept_data(array_keys($_POST));
		$idPermintaan	= $data['id_permintaan'];

		//detail data
		$order = $data['order'];
		$i = 0;
		foreach($order as $row)
		{
			$data_detail[] = array(
				'line_num'					=> $i,
				'kode_produk'				=> $row['kode_produk'],
				'nama_produk'				=> $row['nama_produk'],
				'quantity'					=> $row['quantity'],
				'id_permintaan_detail'		=> isset($row['id_permintaan_detail']) ? $row['id_permintaan_detail'] : 0,
			);
			$i++; 
		}

		//header data
		$data_header = array(
			'permintaan_number'		=> $data['permintaan_number'],
			'revisi'				=> $data['revisi'],
			'tanggal_efektif'		=> isset($data['tanggal_efektif']) ? date('Y-m-d', strtotime($data['tanggal_efektif'])) : '',
			'jumlah_halaman'		=> $data['jumlah_halaman'],
			'contact_person'		=> $data['contact_person'],
			'outlet'				=> $data['outlet'],
			'tanggal_pengiriman'	=> isset($data['tanggal_pengiriman']) ? date('Y-m-d', strtotime($data['tanggal_pengiriman'])) : '',
			'pengiriman'			=> $data['pengiriman']
		);

		$query = $this->Permintaan_model->updateData($data_header, $data_detail, $idPermintaan);
		
		if($query)
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
		else
			$this->session->set_flashdata('error', 'Data gagal disimpan');
		
		redirect('Permintaan');

	}

	function searchItem($itemCode=''){ 
		$itemCode = urldecode($itemCode);
		$retData  = $this->Permintaan_model->search_item($itemCode);
		
		echo json_encode($retData);
	}
}