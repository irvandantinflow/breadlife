<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inadminpage extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Inadminpage_model');	
	}

	public function index(){	
		
	   	$data = array(
	   		'page_action'		=> base_url('Inadminpage/do_login')
		);

	   	$this->load->view('Inadminpage/form_inadminpage', $data);
	}

	function do_login(){
		$user_id = $this->session->userdata('user_id');
		if(!empty($user_id)){
			$this->session->set_flashdata('success', 'Anda sudah login');
			redirect('User');
		};
		
		$this->Inadminpage_model->restrict(true);
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('error', 'Please input Username and/or Password');
			redirect('Inadminpage');
		} else {
			$login = array(
				'username'	=> $this->input->post('username'),
			    'password'	=> $this->input->post('password')
			);
			$return = $this->Inadminpage_model->do_login($login);
			if($return){
				redirect('User');
			} else {
				$this->session->set_flashdata('error', 'Invalid Username or Password');
				redirect('Inadminpage');
			}
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect('Inadminpage');
	}
}