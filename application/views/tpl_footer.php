        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Breadlife
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>


    <!-- Dropzone.js -->
    <script src="<?php echo base_url();?>assets/admin/vendors/dropzone/dist/min/dropzone.min.js"></script>
    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/admin/vendors/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap -->
    <script src="<?php echo base_url();?>assets/admin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url();?>assets/admin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url();?>assets/admin/vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/admin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo base_url();?>assets/admin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>assets/admin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/admin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>assets/admin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/admin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url();?>assets/admin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>assets/admin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>assets/admin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url();?>assets/admin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url();?>assets/admin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url();?>assets/admin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url();?>assets/admin/vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="<?php echo base_url();?>assets/admin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url();?>assets/admin/vendors/maskmoney/jquery.maskMoney.js"></script>

    <!-- Datepicker -->
    <script src="<?php echo base_url();?>assets/admin/js/datepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url();?>assets/admin/build/js/custom.min.js"></script>
    <script src="<?php echo base_url();?>assets/admin/build/js/ekko-lightbox.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url()?>assets/admin/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/admin/js/bootstrap-datetimepicker.id.js" charset="UTF-8"></script>

    <!-- Select2 -->
    <script src="<?php echo base_url();?>assets/admin/vendors/select2/dist/js/select2.full.min.js"></script>
    <script src="<?php echo base_url();?>assets/admin/js/moment/moment.min.js"></script>

    <script src="<?php echo base_url();?>assets/admin/js/accounting.min.js"></script>

    <!-- New Include File -->
    <script src="<?php echo base_url()?>assets/admin/js/uniform/jquery.uniform.js"></script>
    <script src="<?php echo base_url()?>assets/admin/js/uniform/jquery.uniform.min.js"></script>
    <script src="<?php echo base_url()?>assets/admin/js/data-tables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url()?>assets/admin/js/data-tables/DT_bootstrap.js"></script>
    <script src="<?php echo base_url();?>assets/admin/js/jquery.blockui.min.js"></script>
    <script src="<?php echo base_url();?>assets/admin/js/app.js"></script>
    <script src="<?php echo base_url();?>assets/admin/js/permintaan.js"></script>
    <script src="<?php echo base_url();?>assets/admin/js/itemlookup.js"></script>

    <script type="text/javascript">
      $(document).ready(function(){
          var active  = 'Y';
          var notActive  = 'N';
          $('input[name="is_aktif_hide"]').val(active);
      
          $('input[name="is_aktif"]').change(function(){ 
      
              if($('input[name="is_aktif"]:checked').val() == 'Y'){
                  $('input[name="is_aktif_hide"]').val(active);
              } else {
                  $('input[name="is_aktif_hide"]').val(notActive);
              }
          });        
      });
    </script>

    <!-- start datepickier -->
    <script type="text/javascript">
     $('.datepicker').datetimepicker({
        language:  'en',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
        });
    </script>
    <!-- end datepicker --> 

    <!-- Datatables -->
    <script>
      $(document).ready(function() {
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
      });
    </script>
    <!-- /Datatables -->

    <!-- Password match validate -->
    <script type="text/javascript">
      $(document).ready(function(){
        $('#retype_password').blur(function(){
          var pass1 = $('#new_password').val();
          var pass2 = $('#retype_password').val();
          if(pass1 === pass2){
            $('#info_pass_match').css("color","blue");
            $('#info_pass_match').html("");
            $('#info_pass_match').append('Match!');
          } else {
            $('#info_pass_match').css("color","red");
            $('#info_pass_match').html("");
            $('#info_pass_match').append('Not Match!');
          }
        });
      });

      $(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
        event.preventDefault();
        return $(this).ekkoLightbox();
      });
    </script>

    <!-- Select2 -->
    <script>
      $(document).ready(function() {
        $(".select2_single").select2({
          allowClear: true
        });
        $(".select2_group").select2({});
        $(".select2_multiple").select2({
          maximumSelectionLength: 4,
          placeholder: "With Max Selection limit 4",
          allowClear: true
        });
      });
    </script>
    <!-- /Select2 -->
    
  </body>
</html>