<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Breadlife</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>assets/admin/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url();?>assets/admin/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url();?>assets/admin/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo base_url();?>assets/admin/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="<?php echo base_url();?>assets/admin/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/admin/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/admin/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/admin/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/admin/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url();?>assets/admin/build/css/custom.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/admin/build/css/ekko-lightbox.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/admin/vendors/normalize-css/normalize.css" rel="stylesheet">

    <!-- Select2 -->
    <link href="<?php echo base_url();?>assets/admin/vendors/select2/dist/css/select2.min.css" rel="stylesheet">

    <!-- Dropzone.js -->
    <link href="<?php echo base_url();?>assets/admin/vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">

    <!-- Datetime -->
    
    <link href="<?php echo base_url()?>assets/admin/js/datepicker/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('Home')?>" class="site_title"> <span>Breadlife</span></a>
            </div>

            <div class="clearfix"></div>
            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <ul class="nav side-menu">
                    <li class="<?php echo ($active == 'Transaction') ? 'active' : ''; ?>"><a><i class="fa fa-calendar"></i> Transaction <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu" style="<?php echo ($active == 'Transaction') ? 'display:block' : ''; ?>">
                        <li class="<?php echo activate_menu('Permintaan'); ?>"><a href="<?php echo base_url('Permintaan'); ?>">Permintaan</a></li>
                      </ul>
                    </li>
                    <?php if($this->session->userdata('level') == '1') { ?>
                      <li class="<?php echo ($active == 'Master') ? 'active' : ''; ?>"><a><i class="fa fa-cogs"></i> Master <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu" style="<?php echo ($active == 'Master') ? 'display:block' : ''; ?>">
                          <li class="<?php echo activate_menu('User'); ?>"><a href="<?php echo base_url('User'); ?>">User</a></li>
                          <li class="<?php echo activate_menu('Usergroup'); ?>"><a href="<?php echo base_url('Usergroup'); ?>">Usergroup</a></li>
                          <li class="<?php echo activate_menu('Kemasan'); ?>"><a href="<?php echo base_url('Kemasan'); ?>">Kemasan</a></li>
                          <li class="<?php echo activate_menu('Kategori'); ?>"><a href="<?php echo base_url('Kategori'); ?>">Kategori</a></li>
                          <li class="<?php echo activate_menu('Produk'); ?>"><a href="<?php echo base_url('Produk'); ?>">Produk</a></li>
                        </ul>
                      </li>
                    <?php }?>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <?php echo $this->session->userdata('fullname') ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="<?php echo base_url('Inadminpage/logout'); ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->